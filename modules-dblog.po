#
# Pseudo translation of Drupal (modules-dblog)
# Copyright 2007 Konstantin Käfer <kkaefer@gmail.com>
# Generated from files:
#  dblog.admin.inc,v 1.5 2007/12/19 17:45:42 goba
#  dblog.module,v 1.19 2007/12/14 18:08:46 goba
#  dblog.install,v 1.6 2007/11/04 14:33:06 goba
#
msgid ""
msgstr "XXX"
"Project-Id-Version: Drupal 6.0\n"
"POT-Creation-Date: 2007-12-20 11:52+0100\n"
"PO-Revision-Date: 2007-12-23 15:48:34+0100\n"
"Last-Translator: Konstantin Käfer <kkaefer@gmail.com>\n"
"Language-Team: Pseudo translation\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: modules/dblog/dblog.admin.inc:18
msgid "Discard log entries above the following row limit"
msgstr "XXX"

#: modules/dblog/dblog.admin.inc:21
msgid "The maximum number of rows to keep in the database log. Older entries will be automatically discarded. (Requires a correctly configured <a href=\"@cron\">cron maintenance task</a>.)"
msgstr "XXX"

#: modules/dblog/dblog.admin.inc:81;109
msgid "No log messages available."
msgstr "XXX"

#: modules/dblog/dblog.admin.inc:97
msgid "Count"
msgstr "XXX"

#: modules/dblog/dblog.admin.inc:140
msgid "Location"
msgstr "XXX"

#: modules/dblog/dblog.admin.inc:152;220
msgid "Severity"
msgstr "XXX"

#: modules/dblog/dblog.admin.inc:260
msgid "Filter log messages"
msgstr "XXX"

#: modules/dblog/dblog.admin.inc:297
msgid "You must select something to filter by."
msgstr "XXX"

#: modules/dblog/dblog.module:21
msgid "The dblog module monitors your system, capturing system events in a log to be reviewed by an authorized individual at a later time. This is useful for site administrators who want a quick overview of activities on their site. The logs also record the sequence of events, so it can be useful for debugging site errors."
msgstr "XXX"

#: modules/dblog/dblog.module:22
msgid "The dblog log is simply a list of recorded events containing usage data, performance data, errors, warnings and operational information. Administrators should check the dblog report on a regular basis to ensure their site is working properly."
msgstr "XXX"

#: modules/dblog/dblog.module:23
msgid "For more information, see the online handbook entry for <a href=\"@dblog\">Dblog module</a>."
msgstr "XXX"

#: modules/dblog/dblog.module:26
msgid "The dblog module monitors your website, capturing system events in a log to be reviewed by an authorized individual at a later time. The dblog log is simply a list of recorded events containing usage data, performance data, errors, warnings and operational information. It is vital to check the dblog report on a regular basis as it is often the only way to tell what is going on."
msgstr "XXX"

#: modules/dblog/dblog.module:47
msgid "Settings for logging to the Drupal database logs. This is the most common method for small to medium sites on shared hosting. The logs are viewable from the admin pages."
msgstr "XXX"

#: modules/dblog/dblog.module:54
msgid "Recent log entries"
msgstr "XXX"

#: modules/dblog/dblog.module:55
msgid "View events that have recently been logged."
msgstr "XXX"

#: modules/dblog/dblog.module:61
msgid "Top 'page not found' errors"
msgstr "XXX"

#: modules/dblog/dblog.module:62
msgid "View 'page not found' errors (404s)."
msgstr "XXX"

#: modules/dblog/dblog.module:68
msgid "Top 'access denied' errors"
msgstr "XXX"

#: modules/dblog/dblog.module:69
msgid "View 'access denied' errors (403s)."
msgstr "XXX"

#: modules/dblog/dblog.module:0
msgid "dblog"
msgstr "XXX"

#: modules/dblog/dblog.install:25
msgid "Table that contains logs of all system events."
msgstr "XXX"

#: modules/dblog/dblog.install:30
msgid "Primary Key: Unique watchdog event ID."
msgstr "XXX"

#: modules/dblog/dblog.install:36
msgid "The {users}.uid of the user who triggered the event."
msgstr "XXX"

#: modules/dblog/dblog.install:43
msgid "Type of log message, for example \"user\" or \"page not found.\""
msgstr "XXX"

#: modules/dblog/dblog.install:49
msgid "Text of log message to be passed into the t() function."
msgstr "XXX"

#: modules/dblog/dblog.install:55
msgid "Serialized array of variables that match the message string and that is passed into the t() function."
msgstr "XXX"

#: modules/dblog/dblog.install:63
msgid "The severity level of the event; ranges from 0 (Emergency) to 7 (Debug)"
msgstr "XXX"

#: modules/dblog/dblog.install:70
msgid "Link to view the result of the event."
msgstr "XXX"

#: modules/dblog/dblog.install:75
msgid "URL of the origin of the event."
msgstr "XXX"

#: modules/dblog/dblog.install:82
msgid "URL of referring page."
msgstr "XXX"

#: modules/dblog/dblog.install:89
msgid "Hostname of the user who triggered the event."
msgstr "XXX"

#: modules/dblog/dblog.install:95
msgid "Unix timestamp of when event occurred."
msgstr "XXX"

